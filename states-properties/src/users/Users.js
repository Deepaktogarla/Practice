import React, { Component } from 'react';
import User from './User';
export default class Users extends Component {  //we can only use class component for using states mainly extend
  
  state  ={      //we start from here  
      users:[
        {name:"deepak",age:24},
        {name:"yuqiu",age:25},
        {name:"kamil",age:28},
      ]
  }   
  
   //we dd a function to call the event

   increase=()=>{
    const newState = this.state.users.map((user)=>{   //everthing is a function
      const tempUser = user;
      tempUser.age -=1;
      return tempUser;
    });
    this.setState({
      newState
      
    })     //we use this.setstate 

   }


  render() {   //replacing data by this.state
    return (
     
      <div> 
        <button onClick={this.increase} >click me</button>    
        <br/>   
        {
        this.state.users.map((user)=>{ 
          return <User age={user.age}>{user.name}</User>         
      })
        }
      </div>
    )
  }
}

