//es6 functional component
import React from 'react';
const User=(props)=>{           //we use function in this functional componet
    return (                  //we should definitly USE  props when we use function al componet
        <div>{props.children}</div>)
}
export default User;