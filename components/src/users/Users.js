import React, { Component } from 'react';
import User from './User';
export default class Users extends Component {   //class base component so we use class
  render() {   
                                  // we dont neeed props in class component
                              //only has render method in class component
    return (
      <div>                        
      <User>deepak</User>
      <User>yuqiu</User>
      <User>kamil</User>
      </div>
    )
  }
}

