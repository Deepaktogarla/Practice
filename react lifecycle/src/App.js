import React from 'react';

import './App.css';

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
        count: 0
    };
        console.log("initial state");
    
}



  increase(e) {
    this.setState({
        count: this.state.count + 1
    });
}


componentWillMount() {
  console.log("componet will mount");
}
componentDidMount() {
  console.log("componet did mount");
}
componentWillReceiveProps() {
  console.log("componet will receive props");
}
shouldComponentUpdate(nextProps, nextState) {
  console.log("component should update??");
  return true;
}
componentWillUpdate() {
  console.log("componet will update");
}

componentDidUpdate(prevProps, prevState) {
  console.log("componet did update");
}
componentWillUnmount() {
  console.log("componet will unmount");
}

  render() {
    console.log("render method");
    return (
      <div className="App">
        <div>
                <h1>{this.state.count}</h1>
                <button onClick={this.increase.bind(this)}>Count Up!!</button>
            </div>
      </div>
    );
  }

}

 
export default App;
